package com.lei.xiaomkiss.glide;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.lei.xiaomkiss.R;

public class GlideUtils {
    /***
     * 加载普通图片 居中裁剪
     * @param context
     * @param imageView
     * @param path
     */
    public static void displayImage(Context context, ImageView imageView,Object path){
        Glide.with(context)
                .load(path)
                .placeholder(R.drawable.ic_launcher_background)//占位符
                .error(R.drawable.ic_launcher_background)//加载错误
                .transform(new CenterCrop())
                .into(imageView);
    }

    /***
     * 加载圆形头像
     * @param context
     * @param imageView
     * @param path
     */
    public static void displayCircleImage(Context context, ImageView imageView,Object path){
        Glide.with(context)
                .load(path)
                .placeholder(R.drawable.ic_launcher_background)//占位符
                .error(R.drawable.ic_launcher_background)//加载错误
                .transform(new CircleCrop())
                .into(imageView);
    }

    /***
     * 加载圆角头像
     * @param context
     * @param imageView
     * @param path
     * @param raduis
     */

    public static void displayRoudedImage(Context context, ImageView imageView,Object path,int raduis){
        Glide.with(context)
                .load(path)
                .placeholder(R.drawable.ic_launcher_background)//占位符
                .error(R.drawable.ic_launcher_background)//加载错误
                .transform(new CenterCrop(),new RoundedCorners(raduis))
                .into(imageView);
    }


    public static void clear(final Context context){
        Glide.get(context).clearMemory();//运行内存
        new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        Glide.get(context).clearDiskCache();//磁盘缓存，耗时操作
                    }
                }
        ).start();

    }


}
